from django.contrib import admin
from todos.models import Item, Todo
# Register your models here.

class ItemAdmin(admin.ModelAdmin):
    pass


class TodoAdmin(admin.ModelAdmin):
    pass


admin.site.register(Item, ItemAdmin)
admin.site.register(Todo, TodoAdmin)