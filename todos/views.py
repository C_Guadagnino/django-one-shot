from ast import Delete
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from todos.models import Todo
# Create your views here.


class TodoListView(ListView):
    model = Todo
    template_name = "todos/list.html"


class TodoDetailView(DetailView):
    model = Todo
    template_name = "todos/detail.html"


class TodoCreateView(CreateView):
    model = Todo
    template_name = "todos/new.html"
    fields = ['x', 'y', 'z']

    def get_success_url(self):
        return reverse_lazy("todo_detail", args=[self.object.id])


class TodoUpdateView(UpdateView):
    model = Todo
    template_name = "todos/update.html"
    fields = ['x', 'y', 'z']

    def get_success_url(self):
        return reverse_lazy("todo_detail", args=[self.object.id])

class TodoDeleteView(DeleteView):
    model = Todo
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list")
    