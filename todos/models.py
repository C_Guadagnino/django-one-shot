from django.db import models


# Create your models here.


class Todo(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Item(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateField()
    is_completed = models.BooleanField()
    # list = models.ForeignKey(
    #     "todos",
    #     related_name="items", 
    #     on_delete=models.CASCADE,
    # )

    def __str__(self):
        return self.task
