from django.urls import include, path
from todos.views import TodoCreateView, TodoDetailView, TodoDeleteView, TodoListView, TodoUpdateView


urlpatterns = [
    path("", TodoListView.as_view(), name="todos_list"),
    path("<int:pk>/", TodoDetailView.as_view(), name="todos_list"),
    path("new/", TodoCreateView.as_view(), name="todos_new"),
    path("<int:pk>/delete/", TodoDeleteView.as_view(), name="todos_delete"),
    path("<int:pk/update/", TodoUpdateView.as_view(), name="todos_update"),
]